
#ifndef _CP_SSP_
#define _CP_SSP_

#include<CPROCSP/reader/support.h>
#include<CPROCSP/wincspc.h>
#include<CPROCSP/asn1type.h>
#include<CPROCSP/cryptmem.h>

#include<CPROCSP/SSPMap.h>
#include<CPROCSP/CpSChl.h>

//��� ���� ������� runtime - � ���� ����
#ifdef UNDER_CE
#include<CPROCSP/wceaddon.h>
#define SEC_E_CERT_WRONG_USAGE           _HRESULT_TYPEDEF_(0x80090349L)
#endif

#define CPSSP_PACKAGE_COUNT			1

#define CPSSP_PACKAGE_VERSION		1

#define CPSSP_PACKAGE_NAME_A			"CryptoPro SSP"
#define CPSSP_PACKAGE_NAME_W			L"CryptoPro SSP"

#define CPSSP_PACKAGE_COMMENT_A		"CryptoPro Security Package"
#define CPSSP_PACKAGE_COMMENT_W		L"CryptoPro Security Package"

#define SCHANNEL_SPLIT_BY_MESSAGES		0x100

#define SSL3_SSL_SESSION_ID_LENGTH		32
#define SSL3_MAX_SSL_SESSION_ID_LENGTH		32

#define SSL3_MASTER_SECRET_SIZE			48
#define SSL3_RANDOM_SIZE			32
#define SSL3_SESSION_ID_SIZE			32
#define SSL3_RT_HEADER_LENGTH			5

#define SSL3_RT_MAX_EXTRA			(16384)

#define SSL3_RT_MAX_PLAIN_LENGTH		16384
#define SSL3_RT_MAX_COMPRESSED_LENGTH	(1024+SSL3_RT_MAX_PLAIN_LENGTH)
#define SSL3_RT_MAX_ENCRYPTED_LENGTH	(1024+SSL3_RT_MAX_COMPRESSED_LENGTH)
#define SSL3_RT_MAX_PACKET_SIZE		(SSL3_RT_MAX_ENCRYPTED_LENGTH+SSL3_RT_HEADER_LENGTH)
#define SSL3_RT_MAX_DATA_SIZE			(1024*1024)

#define SSL_MAX_MASTER_KEY_LENGTH		48

#define SSL3_ST_CR_CLNT_HELLO   100
#define SSL3_ST_CW_CLNT_HELLO	1
#define SSL3_ST_CR_SRVR_HELLO	2
#define SSL3_ST_CR_CERT		3
#define SSL3_ST_CR_CERT_REQ	32
#define SSL3_ST_CR_SRVR_DONE	33
#define SSL3_ST_CR_OCSP		34
#define SSL3_ST_CW_KEY_EXCH	4
#define SSL3_ST_CW_CHANGE	5
#define SSL3_ST_CW_NEXT_PROTO	52
#define SSL3_ST_CW_FINISHED	6
#define SSL3_ST_CR_FINISHED	7
#define SSL3_ST_CR_HELLO_REQ	8
#define SSL3_ST_CR_SESSION_TICKET 9

#define SSL_ST_RENEGOTIATE	10
#define SSL3_ST_SR_MS_SGC	11
#define SSL3_ST_SW_HELLO_REQ	12

#define SSL3_ST_SR_CLNT_HELLO	20
#define SSL3_ST_SW_SRVR_HELLO	21
#define SSL3_ST_SW_CERT		22
#define SSL3_ST_SW_OCSP		220
#define SSL3_ST_SR_CERT		232
#define SSL3_ST_SR_KEY_EXCH	23
#define SSL3_ST_SR_CERT_VRFY    231
#define SSL3_ST_SR_NEXT_PROTO   233
#define SSL3_ST_SR_FINISHED	24
#define SSL3_ST_SW_CHANGE	25
#define SSL3_ST_SW_FINISHED	26

#define SSL3_ST_NORMAL		0xFF

#define SSL2_MT_CLIENT_HELLO		1

#define SSL3_MT_HELLO_REQUEST			0
#define SSL3_MT_CLIENT_HELLO			1
#define SSL3_MT_SERVER_HELLO			2
#define SSL3_MT_NEW_SESSION_TICKET		4
#define SSL3_MT_CERTIFICATE			11
//#define SSL3_MT_SERVER_KEY_EXCHANGE		12
#define SSL3_MT_CERTIFICATE_REQUEST		13
#define SSL3_MT_SERVER_DONE			14
#define SSL3_MT_CERTIFICATE_VERIFY		15
#define SSL3_MT_CLIENT_KEY_EXCHANGE		16
#define SSL3_MT_FINISHED			20
#define SSL3_MT_CERTIFICATE_STATUS		22
#define SSL3_MT_NEXT_PROTO			67

#define SSL3_MT_CCS				1


#define TLS1_VERSION			0x0301
#define TLS1_VERSION_MAJOR		0x03
#define TLS1_VERSION_MINOR		0x01

#define TLS_MD_CLIENT_FINISH_CONST		"client finished"
#define TLS_MD_SERVER_FINISH_CONST		"server finished"

#define TLS_FINISH_MD_SIZE			12

#define SSL3_AL_WARNING			1
#define SSL3_AL_FATAL			2

#define SSL3_AD_CLOSE_NOTIFY		 0
#define SSL3_AD_UNEXPECTED_MESSAGE	10	/* fatal */
#define SSL3_AD_BAD_RECORD_MAC		20	/* fatal */
#define SSL3_AD_DECOMPRESSION_FAILURE	30	/* fatal */
#define SSL3_AD_HANDSHAKE_FAILURE	40	/* fatal */
#define SSL3_AD_NO_CERTIFICATE		41
#define SSL3_AD_BAD_CERTIFICATE		42
#define SSL3_AD_UNSUPPORTED_CERTIFICATE	43
#define SSL3_AD_CERTIFICATE_REVOKED	44
#define SSL3_AD_CERTIFICATE_EXPIRED	45
#define SSL3_AD_CERTIFICATE_UNKNOWN	46
#define SSL3_AD_ILLEGAL_PARAMETER	47	/* fatal */

#define SSL_SENT_SHUTDOWN	1
#define SSL_RECEIVED_SHUTDOWN	2
#define SSL_WANT_SHUTDOWN	4

#define SSL3_CK_GVO				0x0031
#define SSL3_CK_GVO_KB2				0x0032

#define	SSL3_CT_GVO_SIGN			21
//#define	SSL3_CT_GVO_SIGN_EL		22
#define	SSL3_CT_gostr34102001			22
#define	SSL3_CT_gostr34102012_256		238
#define	SSL3_CT_gostr34102012_512		239

#define TLS_ENDPOINT_BINDING_PREFIX	    "tls-server-end-point:"
#define TLS_UNIQUE_BINDING_PREFIX	    "tls-unique:"

#define SECPKG_ATTR_PATCH_CRED_HANDLE	    0x99
#define SECPKG_ATTR_WSTRUST_GENKEY	    0x98
#define SECPKG_ATTR_WSTRUST_DECRYPTKEY	    0x97

/* extension types */

#define TLS_EXT_SERVER_NAME				0
#define TLS_EXT_STATUS_REQUEST				5
#define TLS_EXT_SESSION_TICKET				0x0023
#define TLS_EXT_HASH_AND_MAC_ALG_SELECT			0xfdE8
#define TLS_EXT_RENEGOTIATION_INFO			0xff01
#define TLS_EXT_NEXT_PROTOCOL_NEGOTIATION		13172
#define TLS_EXT_APPLICATION_LAYER_PROTOCOL_NEGOTIATION	16

typedef struct _SecPkgContext_WSTrustKey
{
    HCRYPTPROV	hCryptProv;
    HCRYPTKEY	hCryptKey;
    unsigned long WrappedKeyLength;
    /*__field_bcount(WrappedKeyLength)*/ unsigned char * WrappedKey;
} SecPkgContext_WSTrustKey, * PSecPkgContext_WSTrustKey;

typedef struct _CPSSP_CREDS_FLAGS_ {
    unsigned int Server:1;
    unsigned int Client:1;
    unsigned int Bad:1;
#ifdef TLS_CONF
    unsigned int Etalon;
#endif
} CPSSP_CREDS_FLAGS, *PCPSSP_CREDS_FLAGS;

typedef struct _CPSSP_SESSION_ *PCPSSP_SESSION;

typedef struct _CPSSP_CREDS_ *PCPSSP_CREDS;

#ifdef _CP_SSP_AP_
typedef struct _CRYPT_USER_DATA{
    PCCERT_CONTEXT  pCertContext;
    HCRYPTPROV	    hCryptProv;
    CSP_BOOL	    bRelease;
}CRYPT_USER_DATA, *PCRYPT_USER_DATA;

typedef struct _ClientCertPolicy
{
    DWORD	    dwFlags;
    GUID	    guidPolicyId;
    DWORD	    dwCertFlags;
    DWORD	    dwUrlRetrievalTimeout;
    CSP_BOOL	    fCheckRevocationFreshnessTime;
    DWORD	    dwRevocationFreshnessTime;
    CSP_BOOL	    fOmitUsageCheck;
    PCCTL_CONTEXT    pCTLContext;
} ClientCertPolicy, *PClientCertPolicy;
#endif 

typedef struct _CPSSP_CREDS_ {
	CPSSP_CREDS_FLAGS			Flags;
	DWORD					dwFlags;
	PCCERT_CONTEXT				pCertContext;
	HCERTSTORE				hRootStore;
	PCCERT_CHAIN_CONTEXT			pChainContext;
        HCRYPTPROV				hCryptProv;
        CSP_BOOL					CPRelease;
        DWORD					dwKeySpec;
	DWORD					dwProvVersion;//0x0306

	/*_LICENSE_CONTROL_*/
	HCRYPTPROV				hPrivProv;
	/*end _LICENSE_CONTROL_*/

	volatile LONG				nRef;
	PCPSSP_SESSION				FirstSession;
	PCPSSP_SESSION				LastSession;
	int					nCachedSessions;
	ASN1SeqOfOctStr				Cred_CA_list;
        PCPSSP_CREDS				prev;
        PCPSSP_CREDS				next;
	DWORD					cSupportedAlgs;  
	ALG_ID*					palgSupportedAlgs;
#ifdef _WIN32
	HANDLE 					hStoreEvent;
#ifdef _CP_SSP_AP_	
	CRYPT_USER_DATA				CryptUserData;
	DWORD					dwCredFormat;	
	ClientCertPolicy			clientCertPolicy;
#endif
	SCHANNEL_MAPPER *			aphMapper[3];
	DWORD					cMappers;
#endif
	CPC_RWLOCK				AccessLock;
} CPSSP_CREDS;

#define SSL_BUFFERS_COUNT 3
#define OCSP_NONCE_CLIENT_LENGTH 8

typedef struct _CPSSP_SESSION_ {
	PCCERT_CONTEXT				pRemoteCertContext;
#ifndef UNDER_CE
	union
	{
	    SEC_CHANNEL_BINDINGS		bindings;
	    BYTE				pbData[sizeof(SEC_CHANNEL_BINDINGS) + sizeof(TLS_ENDPOINT_BINDING_PREFIX) - 1 + 64/*HASH_GR3411_SIZE*/];
	}					EndpointBinding;
	union
	{
	    SEC_CHANNEL_BINDINGS		bindings;
	    BYTE				pbData[sizeof(SEC_CHANNEL_BINDINGS) + sizeof(TLS_UNIQUE_BINDING_PREFIX) - 1 + TLS_FINISH_MD_SIZE];
	}					UniqueBinding;
#endif
	HCERTSTORE				hRemoteCerts;
	SECURITY_STATUS				mapperStatus;
	HANDLE					hToken;
	DWORD					iMapper;
        BYTE					initial_client_random[SSL3_RANDOM_SIZE];
        BYTE					initial_server_random[SSL3_RANDOM_SIZE];

	HCRYPTKEY				hPreMasterSecret;
	HCRYPTKEY				hMasterSecret;
        BYTE 					session_id[SSL3_SSL_SESSION_ID_LENGTH];
        BYTE					session_id_length;
        LPWSTR					target_name;
	CSP_BOOL					is_client;
	volatile LONG				nRef;
	CSP_BOOL					bReused;
	CSP_BOOL					bExtracted;
        PCPSSP_SESSION				next;
        PCPSSP_SESSION				prev;
        time_t					start_time;
        time_t					timeout;
        PCPSSP_CREDS				pCPCredentials;
	ASN1SeqOfOctStr				CA_list;
	DWORD					CipherSuite;
#ifdef _CP_SSP_AP_
	SecPkgContext_ClientCertPolicyResult	clientCertPolicyResult;
#endif
	CPC_RWLOCK				AccessLock;
        BYTE                                    AlpnProtoLen;
        BYTE                                    AlpnProto[MAX_PROTOCOL_ID_SIZE];
        BYTE                                    NpnProtoLen;
        BYTE                                    NpnProto[MAX_PROTOCOL_ID_SIZE];
} CPSSP_SESSION;

typedef struct _CPSSP_CTX_ {
	CPSSP_CTX_FLAGS				Flags;
	CSP_BOOL					fUserHaveNewKeys;
#ifdef _CP_SSP_AP_
	PBYTE					OCSPrequest_extentions;
	DWORD					OCSPrequest_extentions_size;
	LSA_SEC_HANDLE				LsaContextHandle;//�������� ������� shannela
	SCHANNEL_MAPPER *			aphMapper[3];//�������� ����� ������� SpGetContextToken
	LSA_SEC_HANDLE				Lsa64ContextHandle;//�������� lsamode ��� ������ � 64 ������ ���������
	DWORD					iMapper;
	DWORD					cMappers;
	HANDLE					hToken;//UserModeToken
#endif
	DWORD					dwContextState;
	FILETIME				tsStart;
        PCPSSP_SESSION				session;
	PCPSSP_CREDS				pCPCredentialsRd;   // �������� ������
	PCPSSP_CREDS				pCPCredentialsWr;   // �������� ������
	PCPSSP_CREDS				pCPCredentialsFn;   // �������� ������������ (handshake)
	SecBufferDesc				Output;
	SecBufferDesc				Input;
	SecBuffer				Buffers[2*SSL_BUFFERS_COUNT];
        BYTE					client_random[SSL3_RANDOM_SIZE];
        BYTE					server_random[SSL3_RANDOM_SIZE];
	BYTE					finish_md_size;
	BYTE					peer_finish_md_size;
	BYTE					finish_md[TLS_FINISH_MD_SIZE];
	BYTE					peer_finish_md[TLS_FINISH_MD_SIZE];
        HCRYPTHASH				hMasterHash;
	HCRYPTHASH				hFinishHashs[3];
	//3411_94;
	//3411_12_256;
	//3411_12_512;
        HCRYPTKEY				hReadKey;
        HCRYPTKEY				hWriteKey;
        HCRYPTKEY				hReadHMACKey;
        HCRYPTKEY				hWriteHMACKey;
        HCRYPTHASH				hReadHMACHash;
        HCRYPTHASH				hWriteHMACHash;
	CSP_BOOL					fReadHMACCummulative;
	CSP_BOOL					fWriteHMACCummulative;
        BYTE					read_sequence[8];
        BYTE					write_sequence[8];
        DWORD					dwHMACLen;
        PSecBuffer				pLastOutput;
	DWORD					cbLastOutput;
	SECURITY_STATUS				StatusLastOutput;
        DWORD					shutdown;
        DWORD                                   AlpnProtoListLen;
        PBYTE                                   AlpnProtoList;
        DWORD                                   NpnProtoListLen;
        PBYTE                                   NpnProtoList;
	//XXXTODO: ����� ��� ����?
	//DWORD					n_supported_certs;
	//ALG_ID				supported_certs[2];
} CPSSP_CTX, *PCPSSP_CTX;

#define IsNewCipherSuite(CipherSuite) ( TLS_CIPHER_2001 == CipherSuite || TLS_CIPHER_2012 == CipherSuite )
#define Is2012CipherSuite(pCPContext) ( TLS_CIPHER_2012 == pCPContext->session->CipherSuite )

extern void SecSetCPCredsHandle (PSecHandle pSecHandle, PCPSSP_CREDS pCPSecHandle);
extern PCPSSP_CREDS SecGetCPCredsHandle (PSecHandle pSecHandle);
extern void SecSetCPCtxHandle (PSecHandle pSecHandle, PCPSSP_CTX pCPSecHandle);
extern PCPSSP_CTX SecGetCPCtxHandle (PSecHandle pSecHandle);
extern SECURITY_STATUS SecBufferSaveALPNsList( PSecBufferDesc pInput, PCPSSP_CTX pCPContext );
extern void CPInitializeBuffers (PSecBufferDesc InBuffer, PSecBuffer InBuffers, unsigned num, const PSecBufferDesc src);

extern SECURITY_STATUS	MapperStatus2SecStatus (DWORD SecStatus);

void
InitializeSecurityPackage (void);

SECURITY_STATUS
SEC_ENTRY
CPAcceptSecurityContext(
	PCredHandle		phCredential,		/* handle to the credentials*/
	PCtxtHandle		phContext,			/* handle of partially formed context*/
	PSecBufferDesc	pInput,				/* pointer to the input buffers*/
	ULONG			fContextReq,        /* required context attributes*/
	ULONG			TargetDataRep,      /* data representation on the target*/
	PCtxtHandle		phNewContext,		/* receives the new context handle*/
	PSecBufferDesc	pOutput,			/* pointer to the output buffers*/
	PULONG			pfContextAttr,      /* receives the context attributes*/
	PTimeStamp		ptsTimeStamp		/* receives the life span of the security context*/
	);

SECURITY_STATUS
SEC_ENTRY
CPAcquireCredentialsHandleA(
	SEC_CHAR	*pszPrincipal,			/* name of principal*/
	SEC_CHAR	*pszPackage,			/* name of package*/
	ULONG		fCredentialUse,			/* flags indicating use*/
	PLUID		pvLogonID,				/* pointer to logon identifier*/
	PVOID		pAuthData,				/* package-specific data*/
	PVOID		pGetKeyFn,				/* pointer to GetKey function*/
	PVOID		pvGetKeyArgument,		/* value to pass to GetKey*/
	PCredHandle phCredential,			/* credential handle*/
	PTimeStamp	ptsExpiry				/* lifetime of the returned credentials*/
	);

SECURITY_STATUS
SEC_ENTRY
CPAcquireCredentialsHandleW(
	SEC_WCHAR	*pwszPrincipal,			/* name of principal*/
	SEC_WCHAR	*pwszPackage,			/* name of package*/
	ULONG		fCredentialUse,			/* flags indicating use*/
	PLUID		pvLogonID,				/* pointer to logon identifier*/
	PVOID		pAuthData,				/* package-specific data*/
	PVOID		pGetKeyFn,				/* pointer to GetKey function*/
	PVOID		pvGetKeyArgument,		/* value to pass to GetKey*/
	PCredHandle phCredential,			/* credential handle*/
	PTimeStamp	ptsExpiry				/* lifetime of the returned credentials*/
	);

SECURITY_STATUS
SEC_ENTRY
CPApplyControlToken(
	PCtxtHandle		phContext,			/* handle of the context to modify*/
	PSecBufferDesc	pInput				/* input token to apply*/
	);

SECURITY_STATUS
SEC_ENTRY
CPCompleteAuthToken(
	PCtxtHandle		phContext,			/* handle of the context to complete*/
	PSecBufferDesc	pToken				/* token to complete*/
	);

SECURITY_STATUS
SEC_ENTRY
CPDecryptMessage(
	PCtxtHandle phContext,				/* context to use*/
	PSecBufferDesc pMessage,			/* buffer containing the message to decrypt*/
	ULONG MessageSeqNo,					/* expected sequence number*/
	PULONG pfQOP						/* quality of protection*/
	);

SECURITY_STATUS
SEC_ENTRY
CPDeleteSecurityContext(
	PCtxtHandle	phContext				/* handle of the context to delete*/
	);

SECURITY_STATUS
SEC_ENTRY
CPEnumerateSecurityPackagesA(
	PULONG		pcPackages,				/* receives the number of packages*/
	PSecPkgInfoA *ppPackageInfo			/* receives array of information*/
	);

SECURITY_STATUS
SEC_ENTRY
CPEnumerateSecurityPackagesW(
	PULONG			pcPackages,			/* receives the number of packages*/
	PSecPkgInfoW	*ppPackageInfo		/* receives array of information*/
	);

SECURITY_STATUS
SEC_ENTRY
CPEncryptMessage(
	PCtxtHandle		phContext,			/* context to use*/
	ULONG			fQOP,               /* quality of protection*/
	PSecBufferDesc	pMessage,			/* message to encrypt*/
	ULONG			MessageSeqNo        /* message sequence number*/
	);

SECURITY_STATUS
SEC_ENTRY
CPExportSecurityContext(
	PCtxtHandle		phContext,
	ULONG			fFlags,
	PSecBuffer		pPackedContext,
	void SEC_FAR * SEC_FAR * pToken                 // (out, optional) token handle for impersonation
	);

SECURITY_STATUS
SEC_ENTRY
CPFreeContextBuffer(
	PVOID	pvContextBuffer				/* buffer to free*/
	);

SECURITY_STATUS
SEC_ENTRY
CPFreeCredentialsHandle(
	PCredHandle	phCredential			/* handle to free*/
	);

SECURITY_STATUS
SEC_ENTRY
CPImpersonateSecurityContext(
	PCtxtHandle	phContext				/* handle of context to impersonate	*/
	);

SECURITY_STATUS
SEC_ENTRY
CPImportSecurityContextA(
	SEC_CHAR SEC_FAR	*pszPackage, 
	PSecBuffer			pPackedContext, 
	void SEC_FAR		*pToken, 
	PCtxtHandle			phContext 
	);

SECURITY_STATUS
SEC_ENTRY
CPImportSecurityContextW(
	SEC_WCHAR SEC_FAR	*pszPackage, 
	PSecBuffer			pPackedContext, 
	void SEC_FAR		*pToken, 
	PCtxtHandle			phContext 
	);

PSecurityFunctionTableA
SEC_ENTRY
CPInitSecurityInterfaceA(
	void);

PSecurityFunctionTableW
SEC_ENTRY
CPInitSecurityInterfaceW(
	void);

SECURITY_STATUS
SEC_ENTRY
CPInitializeSecurityContextA(
	PCredHandle		phCredential,		/* handle to the credentials*/
	PCtxtHandle		phContext,			/* handle of partially formed context*/
	SEC_CHAR		*pszTargetName,		/* name of the target of the context*/
	ULONG			fContextReq,        /* required context attributes*/
	ULONG			Reserved1,          /* reserved; must be zero*/
	ULONG			TargetDataRep,      /* data representation on the target*/
	PSecBufferDesc	pInput,				/* pointer to the input buffers*/
	ULONG			Reserved2,          /* reserved; must be zero*/
	PCtxtHandle		phNewContext,		/* receives the new context handle*/
	PSecBufferDesc	pOutput,			/* pointer to the output buffers*/
	PULONG			pfContextAttr,      /* receives the context attributes*/
	PTimeStamp		ptsExpiry			/* receives the life span of the security context*/
	);

SECURITY_STATUS
SEC_ENTRY
CPInitializeSecurityContextW(
	PCredHandle		phCredential,		/* handle to the credentials*/
	PCtxtHandle		phContext,			/* handle of partially formed context*/
	SEC_WCHAR		*pwszTargetName,	/* name of the target of the context*/
	ULONG			fContextReq,        /* required context attributes*/
	ULONG			Reserved1,          /* reserved; must be zero*/
	ULONG			TargetDataRep,      /* data representation on the target*/
	PSecBufferDesc	pInput,				/* pointer to the input buffers*/
	ULONG			Reserved2,          /* reserved; must be zero*/
	PCtxtHandle		phNewContext,		/* receives the new context handle*/
	PSecBufferDesc	pOutput,			/* pointer to the output buffers*/
	PULONG			pfContextAttr,      /* receives the context attributes*/
	PTimeStamp		ptsExpiry			/* receives the life span of the security context*/
	);

SECURITY_STATUS
SEC_ENTRY
CPMakeSignature(
	PCtxtHandle		phContext,			/* context to use*/
	ULONG			fQOP,               /* quality of protection*/
	PSecBufferDesc	pMessage,			/* message to sign*/
	ULONG			MessageSeqNo        /* message sequence number*/
	);

SECURITY_STATUS
SEC_ENTRY
CPQueryCredentialsAttributesA(
    PCredHandle		phCredential,        /* Credential to query*/
    unsigned long	ulAttribute,         /* Attribute to query*/
    void SEC_FAR	*pBuffer             /* Buffer for attributes*/
    );

SECURITY_STATUS
SEC_ENTRY
CPQueryCredentialsAttributesW(
    PCredHandle		phCredential,        /* Credential to query*/
    unsigned long	ulAttribute,         /* Attribute to query*/
    void SEC_FAR	*pBuffer             /* Buffer for attributes*/
    );

SECURITY_STATUS
SEC_ENTRY
CPSetCredentialsAttributesA(
    PCredHandle		phCredential,        /* Credential to query*/
    unsigned long	ulAttribute,         /* Attribute to query*/
    void SEC_FAR	*pBuffer,            /* Buffer for attributes*/
    unsigned long	ulBufferSize
    );

SECURITY_STATUS
SEC_ENTRY
CPQueryContextAttributesA(
	PCtxtHandle phContext,				/* handle of the context to query*/
	ULONG		ulAttribute,			/* attribute to query*/
	PVOID		pBuffer					/* buffer for attributes*/
	);

SECURITY_STATUS
SEC_ENTRY
CPQueryContextAttributesW(
	PCtxtHandle phContext,				/* handle of the context to query*/
	ULONG		ulAttribute,			/* attribute to query*/
	PVOID		pBuffer					/* buffer for attributes*/
	);

SECURITY_STATUS
SEC_ENTRY
CPQueryCredentialsAttributes(
	PCredHandle phCredential,			/* credential to query*/
	ULONG		ulAttribute,			/* attribute to query*/
	PVOID		pBuffer					/* buffer that receives attributes*/
	);

SECURITY_STATUS
SEC_ENTRY
CPQuerySecurityContextToken(
	PCtxtHandle phContext,
	HANDLE		*phToken
	);

SECURITY_STATUS
SEC_ENTRY
CPQuerySecurityPackageInfoA(
	SEC_CHAR		*pszPackageName,	/* name of package*/
	PSecPkgInfoA	*ppPackageInfo		/* receives package information*/
	);

SECURITY_STATUS
SEC_ENTRY
CPQuerySecurityPackageInfoW(
	SEC_WCHAR		*pszPackageName,	/* name of package*/
	PSecPkgInfoW	*ppPackageInfo		/* receives package information*/
	);

SECURITY_STATUS
SEC_ENTRY
CPRevertSecurityContext(
	PCtxtHandle	phContext				/* handle of the context being impersonated*/
	);

SECURITY_STATUS
SEC_ENTRY
CP_VerifySignature(
	PCtxtHandle		phContext,			/* context to use*/
	PSecBufferDesc	pMessage,			/* message to verify*/
	ULONG			MessageSeqNo,       /* sequence number*/
	PULONG			pfQOP				/* quality of protection*/
	);

CSP_BOOL CPSSPInitialize (void);

extern CPC_RWLOCK	CPSecHandleTableLock;

#include<CPROCSP/CpSChl.h>

#endif /* _CP_SSP_ */
