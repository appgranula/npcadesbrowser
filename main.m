//
//  main.m
//  npcades-browser
//
//  Created by Кондакова Татьяна Андреевна on 09.02.12.
//  Copyright 2012 CryptoPro Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>
#include "JavascriptCore-dlsym.h"
#include "JSCocoaController.h"

int main(int argc, char *argv[]) {
    
    NSAutoreleasePool * pool = [[NSAutoreleasePool alloc] init];
	
	// Fetch JS symbols
	[JSCocoaSymbolFetcher populateJavascriptCoreSymbols];	
	// Load iPhone bridgeSupport
	[[BridgeSupportController sharedController] loadBridgeSupport:[NSString stringWithFormat:@"%@/iPhone.bridgesupport", [[NSBundle mainBundle] bundlePath]]];
	
    int retVal = UIApplicationMain(argc, argv, nil, nil);
    [pool release];
    return retVal;
}
