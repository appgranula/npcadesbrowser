//
//  NpcadesBrowserViewController.m
//  npcades-browser
//
//  Created by Кондакова Татьяна Андреевна on 21.03.12.
//  Copyright 2012 CryptoPro Ltd. All rights reserved.
//

#import "NpcadesBrowserViewController.h"

extern bool USE_CACHE_DIR;
bool USE_CACHE_DIR = false;

@implementation NpcadesBrowserViewController

- (BOOL) shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation{
	return YES;
}

- (void) viewDidLoad {
//	NSString *defaultPage = @"http://www.cryptopro.ru/sites/default/files/products/cades/demopage/simple.html";
    NSString *defaultPage = @"https://develop.project-osnova.ru/osnova2/pcho/pcho_login.nsf/login.xsp";
	
	toolbar = [[UIToolbar alloc] init];
	[toolbar sizeToFit];
	CGFloat toolbarHeight = [toolbar frame].size.height;
	CGRect appBounds = [UIScreen mainScreen].bounds;
	CGRect appFrame = [UIScreen mainScreen].applicationFrame;
	CGRect webViewFrame = CGRectMake(
									 appBounds.origin.x,
									 appBounds.origin.y + toolbarHeight,
									 appFrame.size.width,
									 appFrame.size.height - toolbarHeight);
	webView =[[NpPluginWebView alloc] initWithFrame: webViewFrame];
	webView.autoresizingMask = UIViewAutoresizingFlexibleWidth |
								UIViewAutoresizingFlexibleHeight;
	[self.view addSubview: webView];
	webView.delegate = self;
	
	if(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
		CPROPane = [[PaneViewController alloc] initWithNibName:@"PaneViewController" bundle:nil];
	else 
		CPROPane = [[PaneViewController alloc] initWithNibName:@"PaneViewControllerIPhone" bundle: nil];
	
	NSString *backImagePath = [[NSBundle mainBundle] pathForResource: @"ArrowBack" ofType: @"png"];
	UIImage *backImage = [[UIImage alloc] initWithContentsOfFile:backImagePath];
	[backButton initWithImage:backImage style:UIBarButtonItemStylePlain target:self action: @selector(goBack)];
	
	NSString *forwardImagePath = [[NSBundle mainBundle] pathForResource: @"ArrowForward" ofType: @"png"];
	UIImage *forwardImage = [[UIImage alloc] initWithContentsOfFile:forwardImagePath];
	[forwardButton initWithImage:forwardImage style:UIBarButtonItemStylePlain target:self action: @selector(goForward)];
	
	activityIndicator.hidesWhenStopped = YES;
	
	addressBar.delegate = self;
	[addressBar setText:defaultPage];
	[webView loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString: defaultPage]]];
}

-(IBAction) goBack{
	[webView goBack];
}

-(IBAction) goForward{
	[webView goForward];	
}

-(void)gotoAddress{
	NSString *address = [addressBar text];
	
	NSMutableArray *prefixes = [NSMutableArray arrayWithCapacity:0];
	[prefixes addObject:@"http://"];
	[prefixes addObject:@"https://"];
	[prefixes addObject:@"ftp://"];
	[prefixes addObject:@"ftps://"];
	
	NSEnumerator *prefixEnum = [prefixes objectEnumerator];
	id element;
	bool hasPrefix = false;
	while(element = [prefixEnum nextObject]){
		if([address hasPrefix:element]){
			hasPrefix = true;
			break;
		}
	}
	if(address && (!hasPrefix) && [address length]){
		address = [@"http://" stringByAppendingString: address];
		[addressBar setText:address];
	}
	
	NSURL *url = [NSURL URLWithString:address];
	//	NSLog([addressBar text]);
	NSURLRequest *requestObj = [NSURLRequest requestWithURL:url];
	
	//Load the request in the UIWebView.
	[webView loadRequest:requestObj];
	[addressBar resignFirstResponder];
}

- (BOOL)webView:(UIWebView *)webView2 shouldStartLoadWithRequest:(NSURLRequest *)request 
 navigationType:(UIWebViewNavigationType)navigationType {
	
	//Capture user link-click
	if (navigationType == UIWebViewNavigationTypeLinkClicked) {
		NSURL *URL = [request URL];	
		if ([[URL scheme] isEqualToString:@"http"]||
			[[URL scheme] isEqualToString:@"https"]||
			[[URL scheme] isEqualToString:@"ftp"]||
			[[URL scheme] isEqualToString:@"ftps"]) {
			[addressBar setText:[URL absoluteString]];
			[self gotoAddress];
		}	 
		return NO;
	}
	
	//process cpnp-js-call
	return [webView webView: webView2 shouldStartLoadWithRequest:request 
								navigationType:navigationType ];
}

-(void)webViewDidStartLoad:(UIWebView *)webView2{
	[activityIndicator startAnimating];
}

- (void)webViewDidFinishLoad:(UIWebView *)webView2 {
	[activityIndicator stopAnimating];
	[backButton setEnabled:[webView canGoBack]];
	[forwardButton setEnabled:[webView canGoForward]];
}

- (void)didReceiveMemoryWarning {
	// Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
	
	// Release any cached data, images, etc that aren't in use.
}

- (void)viewDidUnload {
	// Release any retained subviews of the main view.
	// e.g. self.myOutlet = nil;
}


- (void)dealloc {
    [super dealloc];
}

-(IBAction)startPane{
	UINavigationController * cont = [[UINavigationController alloc] initWithRootViewController:CPROPane];
	[cont setModalPresentationStyle:UIModalPresentationFullScreen];
	[self presentModalViewController:cont animated:YES];
	[cont release];
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField{
	[textField resignFirstResponder];
	[self gotoAddress];
	return NO;
}
@end
