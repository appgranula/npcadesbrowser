//
//  NppException.h
//  npcades-browser
//
//  Created by Кондакова Татьяна Андреевна on 17.02.12.
//  Copyright 2012 CryptoPro Ltd. All rights reserved.
//

#import <Foundation/NSException.h>

@interface NppException : NSException
@end

