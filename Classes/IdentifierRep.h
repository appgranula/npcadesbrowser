/*
 *  IdentifierRep.h
 *  npcades-browser
 *
 *  Created by Кондакова Татьяна Андреевна on 08.02.12.
 *  Copyright 2012 CryptoPro Ltd. All rights reserved.
 *
 */

#ifndef IdentifierRep_h
#define IdentifierRep_h
#include <string>

class IdentifierRep {
public:
    static IdentifierRep* get(int);
    static IdentifierRep* get(std::string);

    static bool isValid(IdentifierRep*);

    bool isString() const { return m_isString; }

    int number() const { return m_isString ? 0 : m_number; }
	std::string string() const { return m_isString ? m_string : 0; }

private:
    IdentifierRep(int number)
        : m_isString(false)
    {
        m_number = number;
    }

    IdentifierRep(std::string name)
        : m_isString(true)
    {
		m_string = name;
    }

    ~IdentifierRep(); // Not implemented

	std::string m_string;
	int m_number;
    bool m_isString;
};

#endif // IdentifierRep_h