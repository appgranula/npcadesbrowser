/*
 *  NpnFuncsImpl.h
 *  npcades-browser
 *
 *  Created by Кондакова Татьяна Андреевна on 08.02.12.
 *  Copyright 2012 CryptoPro Ltd. All rights reserved.
 *
 */

#include "npapi.h"
#include "npruntime.h"

NPIdentifier NpnImpl_GetStringIdentifier(const NPUTF8 *name);
void NpnImpl_GetStringIdentifiers(const NPUTF8 **names, int32_t nameCount, 
								  NPIdentifier *identifiers);
NPIdentifier NpnImpl_GetIntIdentifier(int32_t intid);
bool NpnImpl_IdentifierIsString(NPIdentifier identifier);
NPUTF8 *NpnImpl_UTF8FromIdentifier(NPIdentifier identifier);
int32_t NpnImpl_IntFromIdentifier(NPIdentifier identifier);
NPObject *NpnImpl_CreateObject(NPP npp, NPClass *aClass);
NPObject *NpnImpl_RetainObject(NPObject *npobj);
void NpnImpl_ReleaseObject(NPObject *npobj);
bool NpnImpl_Invoke(NPP npp, NPObject *npobj, NPIdentifier methodName,
					const NPVariant *args, uint32_t argCount, NPVariant *result);
bool NpnImpl_InvokeDefault(NPP npp, NPObject *npobj, const NPVariant *args,
						   uint32_t argCount, NPVariant *result);
bool NpnImpl_Evaluate(NPP npp, NPObject *npobj, NPString *script,
					  NPVariant *result);
bool NpnImpl_GetProperty(NPP npp, NPObject *npobj, NPIdentifier propertyName,
						 NPVariant *result);
bool NpnImpl_SetProperty(NPP npp, NPObject *npobj, NPIdentifier propertyName,
						 const NPVariant *value);
bool NpnImpl_RemoveProperty(NPP npp, NPObject *npobj, NPIdentifier propertyName);
bool NpnImpl_HasProperty(NPP npp, NPObject *npobj, NPIdentifier propertyName);
bool NpnImpl_HasMethod(NPP npp, NPObject *npobj, NPIdentifier methodName);
bool NpnImpl_Enumerate(NPP npp, NPObject *npobj, NPIdentifier **identifier,
					   uint32_t *count);
bool NpnImpl_Construct(NPP npp, NPObject *npobj, const NPVariant *args,
					   uint32_t argCount, NPVariant *result);
void NpnImpl_SetException(NPObject *npobj, const NPUTF8 *message);
const char * NpnImpl_UserAgent(NPP npp);
NPError NpnImpl_GetValue(NPP npp, NPNVariable variable, void * value);
void* NpnImpl_MemAlloc(uint32_t size);
void NpnImpl_ReleaseVariantValue(NPVariant* variant);