//
//  untitled.h
//  npcades-browser
//
//  Created by Кондакова Татьяна Андреевна on 17.02.12.
//  Copyright 2012 CryptoPro Ltd. All rights reserved.
//

char * getDocumentProperty(NPIdentifier propertyName);
void throwJsException(const char* message); 
JSValueRef evaluateInWindow(const char * script);
bool confirmInWindow(const char *message);