/*
 *  npJsSerializations.h
 *  npcades-browser
 *
 *  Created by Кондакова Татьяна Андреевна on 09.02.12.
 *  Copyright 2012 CryptoPro Ltd. All rights reserved.
 *
 */

#include "npfunctions.h"
#include <vector>
#include <string>

std::string argsToString(const NPVariant *vars, int count);
bool npVariantToJson(NPVariant var, id *jsonStr);

bool IdToNPVariant(id val, NPVariant* np_var);
bool NSArrayToNPVariant(NSArray* array, std::vector<NPVariant> &np_var);
bool JSValueRefToNPVariant(JSValueRef js_val, NPVariant * np_var);