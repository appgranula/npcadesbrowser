//
//  untitled.m
//  npcades-browser
//
//  Created by Кондакова Татьяна Андреевна on 15.02.12.
//  Copyright 2012 CryptoPro Ltd. All rights reserved.
//

#import "NpnFuncsImpl.h"
#import "NpnObjCImpl.h"
#import "NppException.h"
#import "JSCocoa.h"
typedef BOOL CSP_BOOL;
#import <CPROCSP/ModalAlert.h>

extern UIWebView * webView;

@implementation NppException
@end

char *  getDocumentProperty(NPIdentifier propertyName){
	NPUTF8 *property = NpnImpl_UTF8FromIdentifier(propertyName);
	NSString * res = [[webView stringByEvaluatingJavaScriptFromString:
				  [NSString stringWithFormat: @"document.%s", property]] retain];
	char * out_str = (char *)malloc(res.length + 1);
	strcpy(out_str, [res cStringUsingEncoding:NSUTF8StringEncoding]);
	out_str[res.length] = '\0';
	free(property);
	return out_str;
}


void throwJsException(const char* message){
	NSException *e = [NppException
					  exceptionWithName:@"NppException"
					  reason:[NSString stringWithFormat: @"%s", message]
					  userInfo:nil];
	@throw e;
}

JSValueRef evaluateInWindow(const char * script){
	JSCocoaController *jsc = [JSCocoaController sharedController];
	[jsc evalJSString:
			[NSString stringWithFormat: 
			 @"function ru_cryptopro_npcades_10_eval_in_window() {return eval(%s)}", script]];
	JSValueRef res = [jsc evalJSString: @"ru_cryptopro_npcades_10_eval_in_window()"];
	return res;
}

bool confirmInWindow(const char *message){
	return [ModalAlert ask:@"" message:
			[NSString stringWithCString:message encoding:NSUTF8StringEncoding]];
}