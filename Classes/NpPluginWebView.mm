//
//  NpPluginWebView.m
//  npcades-browser
//
//  Created by Кондакова Татьяна Андреевна on 08.02.12.
//  Copyright 2012 CryptoPro Ltd. All rights reserved.
//
#define XP_UNIX
#import "NpPluginWebView.h"
#import "npapi.h"
#import "npfunctions.h"
#import "NpnFuncsImpl.h"
#import "npJsSerializations.h"
#import "NppHandles.h"
#import "NppException.h"
#import <stdlib.h>
#import "JSCocoa.h"

UIWebView * webView;

@implementation NpPluginWebView

-(void)initializeNpnFuncs
{
	pNetscapeFuncs.version = (NP_VERSION_MAJOR << 8) | NP_VERSION_MINOR;
	pNetscapeFuncs.size = sizeof(NPNetscapeFuncs);
	
	pNetscapeFuncs.uagent = NpnImpl_UserAgent;
	pNetscapeFuncs.getvalue = NpnImpl_GetValue;
	pNetscapeFuncs.memalloc = NpnImpl_MemAlloc;
	pNetscapeFuncs.memfree = free;
	
	pNetscapeFuncs.getstringidentifier = NpnImpl_GetStringIdentifier;
	pNetscapeFuncs.getstringidentifiers = NpnImpl_GetStringIdentifiers;
	pNetscapeFuncs.getintidentifier = NpnImpl_GetIntIdentifier;
	pNetscapeFuncs.identifierisstring = NpnImpl_IdentifierIsString;
	pNetscapeFuncs.utf8fromidentifier = NpnImpl_UTF8FromIdentifier;
	pNetscapeFuncs.intfromidentifier = NpnImpl_IntFromIdentifier;
	pNetscapeFuncs.createobject = NpnImpl_CreateObject;
	pNetscapeFuncs.retainobject = NpnImpl_RetainObject;
	pNetscapeFuncs.releaseobject = NpnImpl_ReleaseObject;
	pNetscapeFuncs.invoke = NpnImpl_Invoke;
	pNetscapeFuncs.invokeDefault = NpnImpl_InvokeDefault;
//not implemented
//	pNetscapeFuncs.evaluate = NpnImpl_Evaluate;
	pNetscapeFuncs.getproperty = NpnImpl_GetProperty;
	pNetscapeFuncs.setproperty = NpnImpl_SetProperty;
	pNetscapeFuncs.removeproperty = NpnImpl_RemoveProperty;
	pNetscapeFuncs.hasproperty = NpnImpl_HasProperty;
	pNetscapeFuncs.hasmethod = NpnImpl_HasMethod;
	pNetscapeFuncs.enumerate = NpnImpl_Enumerate;
	pNetscapeFuncs.construct = NpnImpl_Construct;
	pNetscapeFuncs.releasevariantvalue = NpnImpl_ReleaseVariantValue;
	pNetscapeFuncs.setexception = NpnImpl_SetException;
}

- (id)initWithFrame:(CGRect)frame 
{
	webView = self;
	if (self = [super initWithFrame:frame]) {
		[self initializeNpnFuncs];
		NP_Initialize(&pNetscapeFuncs, &pPluginFuncs);
		npp = (NPP)malloc(sizeof(*npp));
		NPP_New("application/x-cades", npp, 0, 0, 0, 0, 0);
		NPP_GetValue(npp, NPPVpluginScriptableNPObject, (void *)&scriptableMainObj);

		self.delegate = self;
    
    // Instanciate JSON parser library
		json = [ SBJSON new ];
    
		self.scalesPageToFit = YES;
	}
	return self;
}



// This selector is called when something is loaded in our webview
// By something I don't mean anything but just "some" :
//  - main html document
//  - sub iframes document
//
// But all images, xmlhttprequest, css, ... files/requests doesn't generate such events :/
- (BOOL)webView:(UIWebView *)webView2 
	      shouldStartLoadWithRequest:(NSURLRequest *)request 
	      navigationType:(UIWebViewNavigationType)navigationType {
	
	NSString *requestString = [[request URL] absoluteString];
  
	NSLog(@"request : %@",requestString);
  
	if ([requestString hasPrefix:@"cpnp-js-call:"]) {
    
		NSArray *components = [requestString componentsSeparatedByString:@":"];
    
		NSString *function = (NSString*)[components objectAtIndex:1];
		int callbackId = [((NSString*)[components objectAtIndex:2]) intValue];
		NSString *argsAsString = [(NSString*)[components objectAtIndex:3] 
                                stringByReplacingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
		NSArray *args = (NSArray*)[json objectWithString:argsAsString error:nil];
    
		[self handleCall:function callbackId:callbackId args:args];
    
		return NO;
	}
	return YES;
}

// Call this function when you have results to send back to javascript callbacks
// callbackId : int comes from handleCall function
// args: list of objects to send to the javascript callback
- (void)returnResult:(int)callbackId exception:(NSString *)message args:(NPVariant) arg;
{
	if (callbackId==0) return;
	
	NSMutableArray *resultArray = [[NSMutableArray alloc] init];

	if(message){
		[resultArray addObject:message];
	}
	else{
		id jsonArg;
		if(npVariantToJson(arg, &jsonArg)){
			[resultArray addObject:[NSNull null]];
			if(jsonArg)
				[resultArray addObject: jsonArg];
		}
		else{
			//set exception
			[resultArray addObject:@"method returned incorrect object"];
		}
	}
	
	NSString *resultArrayString = [json stringWithObject:resultArray allowScalar:YES error:nil];
	NSLog(resultArrayString);
  
	[self stringByEvaluatingJavaScriptFromString: 
			[NSString stringWithFormat:@"ru_cryptopro_npcades_10_native_bridge.resultForCallback(%d,%@);",callbackId,resultArrayString]];
}

// Implement all native function in this one, by matching 'functionName' and parsing 'args'
// Use 'callbackId' with 'returnResult' selector when you get some results to send back to javascript
- (void)handleCall:(NSString*)functionName callbackId:(int)callbackId args:(NSArray*)args
{
  if ([functionName isEqualToString:@"CreateObject"]) {
	  [self CreateObject: callbackId args: args];
  } else if ([functionName isEqualToString:@"NPN_Invoke"]){
	  [self Invoke:callbackId args:args];
  } else if ([functionName isEqualToString:@"NPN_GetProperty"]){
	  [self GetProperty:callbackId args:args];
  } else if ([functionName isEqualToString:@"NPN_SetProperty"]){
	  [self SetProperty:callbackId args:args];
  } else {
	  NSLog(@"Unimplemented method '%@'",functionName);
  }
}

-(void) CreateObject: (int)callbackId args:(NSArray*)args{
	if([args count] != 1)
		return;
	NPVariant ret;
	NPVariant objType;
	STRINGZ_TO_NPVARIANT([(NSString *)[args objectAtIndex:0] cStringUsingEncoding: NSUTF8StringEncoding], objType);
	NPIdentifier methodNameId = NPN_GetStringIdentifier("CreateObject");
	
	NSString* message=nil;
	bool success = false;
	@try{
		success = NPN_Invoke(npp, scriptableMainObj, methodNameId, &objType, 1, &ret);
	}
	@catch(NppException *e){
		message = [e reason];
	}
	if(message == nil && !success)
		message = @"function failed.";
	
	currentCallbackId = callbackId;
	[self returnResult: currentCallbackId exception: message args:ret];
}

-(void) Invoke: (int)callbackId args:(NSArray*)args{
	if([args count] < 2)
		return;
	NppHandleType handle = (NppHandleType)[[args objectAtIndex:0] intValue];
	NPObject * pObj;
	getPointerFromHandleId(handle, &pObj);
	NPIdentifier methodNameId = NPN_GetStringIdentifier([(NSString*)[args objectAtIndex:1] cString]);
	
	std::vector<NPVariant> invoke_args;
	if([args count] >= 3){
		if([[args objectAtIndex:2] isKindOfClass: [NSArray class]]){
			NSArrayToNPVariant((NSArray *)[args objectAtIndex:2], invoke_args);
		}
		else if(![[args objectAtIndex:2] isKindOfClass:[NSNull class]]) {
			NPVariant temp;
			if(IdToNPVariant([args objectAtIndex:2], &temp))
				invoke_args.push_back(temp);
		}

	}
	
	NSString* message=nil;
	NPVariant ret;
	bool success = false;
	@try{
		success = NPN_Invoke(npp, pObj, methodNameId, &invoke_args[0], invoke_args.size(), &ret);
	}
	@catch(NppException *e){
		message = [e reason];
	}
	if(message == nil && !success)
		message = @"function failed.";

	currentCallbackId=callbackId;
	[self returnResult: currentCallbackId exception: message args:ret];
}

-(void) GetProperty: (int)callbackId args:(NSArray*)args{
	if([args count] < 2)
		return;
	NppHandleType handle = (NppHandleType)[[args objectAtIndex:0] intValue];
	NPObject * pObj;
	getPointerFromHandleId(handle, &pObj);
	NPIdentifier propertyNameId = NPN_GetStringIdentifier([(NSString*)[args objectAtIndex:1] cString]);
	
	NSString* message=nil;
	NPVariant ret;
	@try{
		NPN_GetProperty(npp, pObj, propertyNameId, &ret);
	}
	@catch(NppException *e){
		message = [e reason];
	}

	currentCallbackId=callbackId;
	[self returnResult: currentCallbackId exception: message args:ret];
}

-(void) SetProperty: (int)callbackId args:(NSArray*)args{
	if([args count] < 3)
		return;
	NppHandleType handle = (NppHandleType)[[args objectAtIndex:0] intValue];
	NPObject * pObj;
	getPointerFromHandleId(handle, &pObj);
	NPIdentifier propertyNameId = NPN_GetStringIdentifier([(NSString*)[args objectAtIndex:1] cString]);
	
	NPVariant value;
	NSString* message=nil;
	if(!IdToNPVariant([args objectAtIndex:2], &value)){
		message = @"Can't parse input arguments.";
	}
	
	@try{
		NPN_SetProperty(npp, pObj, propertyNameId, &value);
	}
	@catch(NppException *e){
		message = [e reason];
	}
	
	currentCallbackId=callbackId;
	NPVariant ret;
	VOID_TO_NPVARIANT(ret);
	[self returnResult: currentCallbackId exception: message args:ret];
}
@end
