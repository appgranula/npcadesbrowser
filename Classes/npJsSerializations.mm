/*
 *  npJsSerializations.cpp
 *  npcades-browser
 *
 *  Created by Кондакова Татьяна Андреевна on 09.02.12.
 *  Copyright 2012 CryptoPro Ltd. All rights reserved.
 *
 */

#include "npJsSerializations.h"
#include "NppHandles.h"
#include "NpnFuncsImpl.h"
#include <map>
#include <stdlib.h>
#include "JSCocoa.h"
#import "NPJavaScriptObject.h"

std::string NpSerializeObject(NPObject *obj)
{
	std::string res;
	if(!obj)
		return res;
	
	bool isJSObject = false;
	if(obj->_class->hasProperty(obj, NpnImpl_GetStringIdentifier("isJSObject"))){
		NPVariant is_js;
		obj->_class->getProperty(obj, NpnImpl_GetStringIdentifier("isJSObject"), &is_js);
		if(NPVARIANT_IS_BOOLEAN(is_js))
			if(NPVARIANT_TO_BOOLEAN(is_js))
				isJSObject = true;
	}
	
	if(isJSObject){
		JSContextRef js_context = JSGlobalContextCreate(NULL);
		NPVariant js_obj_handle;
		if(!NpnImpl_GetProperty(0, obj, NpnImpl_GetStringIdentifier("handle"), &js_obj_handle))
			return res;
		JSValueRef ref = (JSValueRef)(NPVARIANT_TO_INT32(js_obj_handle));
		JSStringRef str_js_obj = JSValueCreateJSONString(js_context, ref, 1, 0);
		char *buffer;
		int size = JSStringGetLength(str_js_obj) + 1;
		buffer = (char *)malloc(size);
		JSStringGetUTF8CString(str_js_obj, buffer, size);
		res = std::string(buffer);
		free(buffer);
	}
	else{
		//get handle
		PNPPHandle pNewHandle;
		if(!addObjectToHandleTable(obj, &pNewHandle))
			return std::string();
	
		//enumerate methods and properties
		NPIdentifier* identifiers;
		uint32_t identifiers_count;
		if(!NpnImpl_Enumerate(0, obj, &identifiers, &identifiers_count))
			return std::string();
	
		//everything is ok, let's construct string
		res = "{_CPNP_handle:" + nppHandleToString(pNewHandle->hId) + ",";
		for(int i = 0; i < identifiers_count; ++i){
			if(obj->_class->hasProperty(obj, identifiers[i])){
				char * ident_name = NpnImpl_UTF8FromIdentifier(identifiers[i]);
				res += "get " + std::string(ident_name) + "() {\n ";
				res += "return call_ru_cryptopro_npcades_10_native_bridge(\"NPN_GetProperty\",\n";
				res += "[this._CPNP_handle, \"" + std::string(ident_name) + "\"]);\n},\n";
				res += "set " + std::string(ident_name) + "(val) {\n ";
				res += "return call_ru_cryptopro_npcades_10_native_bridge(\"NPN_SetProperty\",\n";
				res += "[this._CPNP_handle, \"" + std::string(ident_name) + "\", val]);\n},\n";
				free(ident_name);
			}
			if(obj->_class->hasMethod(obj, identifiers[i])){
				char * ident_name = NpnImpl_UTF8FromIdentifier(identifiers[i]);
				res += std::string(ident_name) + ": function() {\n";
				res += "var array = Array.prototype.slice.call(arguments, 0);";
				res += " return call_ru_cryptopro_npcades_10_native_bridge(\"NPN_Invoke\",";
				res += "[this._CPNP_handle, \"" + std::string(ident_name) + "\", array]);\n},\n";
				free(ident_name);
			}
		}
		res +="};";
		NPN_MemFree(identifiers);
	}
	return res;
}

std::string NPVariantToString(const NPVariant var){
	switch (var.type) {
		case NPVariantType_Object:{
			NPObject *pNpObj = NPVARIANT_TO_OBJECT(var);
			return NpSerializeObject(pNpObj);
		}
		case NPVariantType_String:{
			NPString npstr_out = NPVARIANT_TO_STRING(var);
			char *utf8string = (char *)malloc(npstr_out.UTF8Length + 1);
			strncpy(utf8string, npstr_out.UTF8Characters, npstr_out.UTF8Length);
			utf8string[npstr_out.UTF8Length] = '\0';
			std::string res = std::string(utf8string);
			free(utf8string);
			return res;
		}
		case NPVariantType_Void:{
			return std::string();
		}
		case NPVariantType_Null:{
			return std::string();
		}
		case NPVariantType_Bool:{
			bool bool_out = NPVARIANT_TO_BOOLEAN(var);
			if(bool_out)
				return "true";
			else
				return "false";
		}
		case NPVariantType_Int32:{
			int int_out = NPVARIANT_TO_INT32(var);
			char buffer[32];
			snprintf(buffer, 32, "%d", int_out);
			return std::string(buffer);
		}
		case NPVariantType_Double:{
			double double_out = NPVARIANT_TO_DOUBLE(var);
			char buffer[32];
			snprintf(buffer, 32, "%f", double_out);
			return std::string(buffer);
		}		
	}
	return std::string();
}

std::string argsToString(const NPVariant *vars, int count){
	if(count < 1)
		return std::string();
	std::string res;
	res = NPVariantToString(vars[0]);
	for(int i = 1; i < count; ++i){
		res += ", " + NPVariantToString(vars[i]);
	}
	return res;
}

bool npVariantToJson(NPVariant var, id *jsonStr){
	switch (var.type) {
		case NPVariantType_Object:{
			std::string str_out=NpSerializeObject(NPVARIANT_TO_OBJECT(var));
			*jsonStr = [NSString stringWithCString:str_out.c_str() encoding: NSUTF8StringEncoding];
			return true;
		}
		case NPVariantType_String:{
			NPString npstr_out = NPVARIANT_TO_STRING(var);
			char *utf8string = (char *)malloc(npstr_out.UTF8Length + 1);
			strncpy(utf8string, npstr_out.UTF8Characters, npstr_out.UTF8Length);
			utf8string[npstr_out.UTF8Length] = '\0';
			NSString * nsUtf8String = [NSString stringWithFormat: @"'%@'",
									   [NSString stringWithUTF8String:utf8string]];
			nsUtf8String = [nsUtf8String stringByReplacingOccurrencesOfString:
							@"\\"
																   withString:@"\\\\"];
			nsUtf8String = [nsUtf8String stringByReplacingOccurrencesOfString:
							@"\r\n"
																   withString:@"\\\\\\r\\n"];
			nsUtf8String = [nsUtf8String stringByReplacingOccurrencesOfString:
							@"\n"
																   withString:@"\\\\\\n"];
			*jsonStr = [NSString stringWithString:nsUtf8String];
			free(utf8string);
			return true;
		}
		case NPVariantType_Void:
			*jsonStr = nil;
			return true;
		case NPVariantType_Null:
			*jsonStr = [NSNull null];
			return true;
		case NPVariantType_Bool:
			*jsonStr = [NSNumber numberWithBool: NPVARIANT_TO_BOOLEAN(var)];
			return true;
		case NPVariantType_Int32:{
			*jsonStr = [NSNumber numberWithInt: NPVARIANT_TO_INT32(var)];
			return true;
		}
		case NPVariantType_Double:{
			*jsonStr = [NSNumber numberWithDouble: NPVARIANT_TO_DOUBLE(var)];
			return true;
		}		
		default:
			return false;
	}
}

bool IdToNPVariant(id val, NPVariant* np_var){
	if ([val isKindOfClass:[NSDictionary class]]){
		for( id key in (NSDictionary *)val){
			if([(NSString *)key compare:@"_CPNP_handle"] == NSOrderedSame){
				NppHandleType handle = (NppHandleType)[[(NSDictionary *)val objectForKey:key] intValue];
				NPObject * pObj;
				getPointerFromHandleId(handle, &pObj);
				OBJECT_TO_NPVARIANT(pObj, *np_var);
				return true;
			}
		}
		return false;
	} else if ([val isKindOfClass:[NSString class]]) {
		STRINGZ_TO_NPVARIANT([(NSString *)val cStringUsingEncoding:NSUTF8StringEncoding], *np_var);
	} else if ([val isKindOfClass:[NSNumber class]]) {
		NSLog(@"%s %s", [val objCType], @encode(double));
		if (strcmp([val objCType],@encode(BOOL)) == 0){
			BOOLEAN_TO_NPVARIANT([(NSNumber *)val boolValue], *np_var);
		} else if (strcmp([val objCType], @encode(double)) == 0) {
			DOUBLE_TO_NPVARIANT([(NSNumber *)val doubleValue], *np_var);
		} else {
			INT32_TO_NPVARIANT([(NSNumber *)val intValue], *np_var);
		}
	} else if ([val isKindOfClass:[NSNull class]]) {
		NULL_TO_NPVARIANT(*np_var);
	} else {
		return false;
	}
	return true;
}

bool NSArrayToNPVariant(NSArray* array, std::vector<NPVariant> &np_var)
{
	int count = [array count];
	for (int i = 0; i < count; ++i) {
		NPVariant temp;
		if(!IdToNPVariant([array objectAtIndex:i], &temp))
			return false;
		np_var.push_back(temp);
	}
	return true;
}

bool JSValueRefToNPVariant(JSValueRef js_val, NPVariant * np_var){
	JSContextRef js_context = JSGlobalContextCreate(NULL);
	JSType js_val_type = JSValueGetType(js_context, js_val);
	switch (js_val_type) {
		case kJSTypeUndefined:
			VOID_TO_NPVARIANT(*np_var);
			return true;
		case kJSTypeNull:
			NULL_TO_NPVARIANT(*np_var);
			return true;
		case kJSTypeBoolean:{
			bool bool_val = JSValueToBoolean(js_context, js_val);
			BOOLEAN_TO_NPVARIANT(bool_val, *np_var);
			return true;
		}
		case kJSTypeNumber:{
			JSValueRef ex = 0;
			double double_val = JSValueToNumber(js_context, js_val, &ex);
			if(ex)
				return false;
			DOUBLE_TO_NPVARIANT(double_val, *np_var);
			return true;
		}
		case kJSTypeString:{
			JSValueRef ex = 0;
			JSStringRef string_val = JSValueToStringCopy(js_context, js_val, &ex);
			if(ex)
				return false;
			//`\0` не нужен
			size_t string_len = JSStringGetMaximumUTF8CStringSize(string_val) - 1;
			//освободится в ReleaseVariantValue
			char *buffer = (char *)NpnImpl_MemAlloc(string_len);
			if(!buffer)
				return false;
			JSStringGetUTF8CString(string_val, buffer, string_len);
			STRINGN_TO_NPVARIANT(buffer, string_len, *np_var);
			return true;
		}
		case kJSTypeObject:{
			NPObject * obj_val = NpnImpl_CreateObject(0, &NPJavaScriptObject_NPClass);
			if(!obj_val)
				return false;
			NPVariant js_handle;
			INT32_TO_NPVARIANT((int)js_val, js_handle);
			if(!NpnImpl_SetProperty(0, obj_val, NpnImpl_GetStringIdentifier("handle"), &js_handle))
				return false;
			OBJECT_TO_NPVARIANT(obj_val, *np_var);
			return true;
		}
		default:
			return false;
	}
	return false;
}