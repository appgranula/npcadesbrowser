/*
 *  NPJavaScriptObject.cpp
 *  npcades-browser
 *
 *  Created by Кондакова Татьяна Андреевна on 14.03.12.
 *  Copyright 2012 CryptoPro Ltd. All rights reserved.
 *
 */

#import "NPJavaScriptObject.h"
#import "NpnFuncsImpl.h"
#import "JSCocoa.h"
#import "npJsSerializations.h"

bool NPJavaScriptObject::HasProperty(NPIdentifier name){
	if (NpnImpl_GetStringIdentifier("handle") == name)
		return true;
	if (NpnImpl_GetStringIdentifier("isJSObject") == name)
		return true;
	return false;
}

bool NPJavaScriptObject::SetProperty(NPIdentifier name, const NPVariant *Value){
	if(NpnImpl_GetStringIdentifier("handle") == name)
		if(NPVARIANT_IS_INT32(*Value)){
			handle = NPVARIANT_TO_INT32(*Value);
			return true;
		}
	return false;
}

bool NPJavaScriptObject::GetProperty(NPIdentifier name, NPVariant *Value){
	if(NpnImpl_GetStringIdentifier("isJSObject") == name){
		BOOLEAN_TO_NPVARIANT(true,*Value);
		return true;
	}
	if(NpnImpl_GetStringIdentifier("handle") == name){
		INT32_TO_NPVARIANT(handle,*Value);
		return true;
	}
	return false;
}

void NPJavaScriptObject::_Deallocate(NPObject *npobj){}
void NPJavaScriptObject::_Invalidate(NPObject *npobj){}

bool NPJavaScriptObject::_HasMethod(NPObject *npobj, NPIdentifier name){
	return false;
}

bool NPJavaScriptObject::_Invoke(NPObject *npobj, NPIdentifier name,
					const NPVariant *args, uint32_t argCount,
					NPVariant *result){
	
	JSCocoaController *jsc = [JSCocoaController sharedController];
	std::string method_args = argsToString(args, argCount);
	
	char * method_name = NpnImpl_UTF8FromIdentifier(name);
	JSValueRef js_res = [jsc anonEval:
		[NSString stringWithFormat:@"return this.%s(%s)", 
		 method_name,
		 method_args.c_str()]
		 withThis:(JSValueRef)(((NPJavaScriptObject *)npobj)->handle)] ;
	free(method_name);
	return JSValueRefToNPVariant(js_res, result);
}

bool NPJavaScriptObject::_InvokeDefault(NPObject *npobj, const NPVariant *args,
						   uint32_t argCount, NPVariant *result){
	return false;
}

bool NPJavaScriptObject::_HasProperty(NPObject * npobj, NPIdentifier name){
	return ((NPJavaScriptObject *)npobj)->HasProperty(name);
}

bool NPJavaScriptObject::_GetProperty(NPObject *npobj, NPIdentifier name,
						 NPVariant *result){
	return ((NPJavaScriptObject *)npobj)->GetProperty(name, result);
}

bool NPJavaScriptObject::_SetProperty(NPObject *npobj, NPIdentifier name,
						 const NPVariant *value){
	return ((NPJavaScriptObject *)npobj)->SetProperty(name, value);
}

bool NPJavaScriptObject::_RemoveProperty(NPObject *npobj, NPIdentifier name){
	return false;
}

bool NPJavaScriptObject::_Enumerate(NPObject *npobj, NPIdentifier **identifier,
					   uint32_t *count){
	return false;
}

bool NPJavaScriptObject::_Construct(NPObject *npobj, const NPVariant *args,
					   uint32_t argCount, NPVariant *result){
	return false;
}
