/*
 *  NPJavaScriptObject.h
 *  npcades-browser
 *
 *  Created by Кондакова Татьяна Андреевна on 14.03.12.
 *  Copyright 2012 CryptoPro Ltd. All rights reserved.
 *
 */
#include "npfunctions.h"

class NPJavaScriptObject: public NPObject
{
public:
	bool HasProperty(NPIdentifier name);
	bool SetProperty(NPIdentifier name, const NPVariant *Value);
	bool GetProperty(NPIdentifier name, NPVariant *Value);
	
	static void _Deallocate(NPObject *npobj);
	static void _Invalidate(NPObject *npobj);
	static bool _HasMethod(NPObject *npobj, NPIdentifier name);
	static bool _Invoke(NPObject *npobj, NPIdentifier name,
						const NPVariant *args, uint32_t argCount,
						NPVariant *result);
	static bool _InvokeDefault(NPObject *npobj, const NPVariant *args,
							   uint32_t argCount, NPVariant *result);
	static bool _HasProperty(NPObject * npobj, NPIdentifier name);
	static bool _GetProperty(NPObject *npobj, NPIdentifier name,
							 NPVariant *result);
	static bool _SetProperty(NPObject *npobj, NPIdentifier name,
							 const NPVariant *value);
	static bool _RemoveProperty(NPObject *npobj, NPIdentifier name);
	static bool _Enumerate(NPObject *npobj, NPIdentifier **identifier,
						   uint32_t *count);
	static bool _Construct(NPObject *npobj, const NPVariant *args,
						   uint32_t argCount, NPVariant *result);
		
	static const bool isJSObject = true;
	int handle;
};

__attribute__ ((weak)) NPClass NPJavaScriptObject_NPClass = {                         
	NP_CLASS_STRUCT_VERSION_CTOR,                                               
	0,                                                                       
	NPJavaScriptObject::_Deallocate,                                    
	NPJavaScriptObject::_Invalidate,                                    
	NPJavaScriptObject::_HasMethod,                                     
	NPJavaScriptObject::_Invoke,                                        
	NPJavaScriptObject::_InvokeDefault,                                 
	NPJavaScriptObject::_HasProperty,                                   
	NPJavaScriptObject::_GetProperty,                                   
	NPJavaScriptObject::_SetProperty,                                   
	NPJavaScriptObject::_RemoveProperty,                                
	NPJavaScriptObject::_Enumerate,                                     
	NPJavaScriptObject::_Construct                                      
};