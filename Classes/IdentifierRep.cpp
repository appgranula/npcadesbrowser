/*
 *  IdentifierRep.cpp
 *  npcades-browser
 *
 *  Created by Кондакова Татьяна Андреевна on 08.02.12.
 *  Copyright 2012 CryptoPro Ltd. All rights reserved.
 *
 */

#include "IdentifierRep.h"
#include <map>
#include <set>

typedef std::set<IdentifierRep*> IdentifierSet;

static IdentifierSet& identifierSet()
{
    static IdentifierSet& identifierSet = *new IdentifierSet();
    return identifierSet;
}

typedef std::map<int, IdentifierRep*> IntIdentifierMap;

static IntIdentifierMap& intIdentifierMap()
{
    static IntIdentifierMap& intIdentifierMap = *new IntIdentifierMap();
    return intIdentifierMap;
}

IdentifierRep* IdentifierRep::get(int intID)
{
    if (intID == 0 || intID == -1) {
        static IdentifierRep* negativeOneAndZeroIdentifiers[2];

        IdentifierRep* identifier = negativeOneAndZeroIdentifiers[intID + 1];
        if (!identifier) {
            identifier = new IdentifierRep(intID);

            negativeOneAndZeroIdentifiers[intID + 1] = identifier;
        }

        return identifier;
    }

	std::pair<IntIdentifierMap::iterator, bool> result = 
				intIdentifierMap().insert(std::pair<int, IdentifierRep*>(intID, 0));
    if (result.second) {
        result.first->second = new IdentifierRep(intID);

        identifierSet().insert(result.first->second);
    }

    return result.first->second;
}

typedef std::map<std::string, IdentifierRep*> StringIdentifierMap;

static StringIdentifierMap& stringIdentifierMap()
{
    static StringIdentifierMap& stringIdentifierMap = *new StringIdentifierMap();
    return stringIdentifierMap;
}

IdentifierRep* IdentifierRep::get(std::string name)
{
    std::pair<StringIdentifierMap::iterator, bool> result = 
				stringIdentifierMap().insert(std::pair<std::string, IdentifierRep*>(name, 0));
    if (result.second) {
        result.first->second = new IdentifierRep(name);

        identifierSet().insert(result.first->second);
    }

    return result.first->second;
}

bool IdentifierRep::isValid(IdentifierRep* identifier)
{
    return (bool)identifierSet().count(identifier);
}
