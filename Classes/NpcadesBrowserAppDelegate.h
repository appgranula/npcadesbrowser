//
//  NpcadesBrowserAppDelegate.h
//  npcades-browser
//
//  Created by Кондакова Татьяна Андреевна on 21.03.12.
//  Copyright 2012 CryptoPro Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>

@class NpcadesBrowserViewController;

@interface NpcadesBrowserAppDelegate : NSObject <UIApplicationDelegate> {
    UIWindow *window;
    NpcadesBrowserViewController *viewController;
}

@property (nonatomic, retain) IBOutlet UIWindow *window;
@property (nonatomic, retain) IBOutlet NpcadesBrowserViewController *viewController;

@end

