/*
 *  NppHandles.h
 *  npcades-browser
 *
 *  Created by Кондакова Татьяна Андреевна on 09.02.12.
 *  Copyright 2012 CryptoPro Ltd. All rights reserved.
 *
 */

#include <vector>
#include <string>
#include "npapi.h"
#include "npfunctions.h"
//пока без удаления и без параллельной записи, потом можно доработать
typedef long NppHandleType;

typedef struct NPPHandle_ {
	NppHandleType hId;
	NPObject * pObj;
} NPPHandle;

typedef struct NPPHandle_ *PNPPHandle;

bool addObjectToHandleTable(NPObject *pObj, PNPPHandle *ppNewHandle);
bool getPointerFromHandleId(NppHandleType hId, NPObject **pObj);
std::string nppHandleToString(NppHandleType handle);