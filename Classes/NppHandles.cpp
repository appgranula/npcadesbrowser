/*
 *  NppHandles.cpp
 *  npcades-browser
 *
 *  Created by Кондакова Татьяна Андреевна on 09.02.12.
 *  Copyright 2012 CryptoPro Ltd. All rights reserved.
 *
 */

#include "NppHandles.h"

typedef std::vector<NPPHandle> NPPHansleSet;

static NPPHansleSet nppHandleTable;

bool addObjectToHandleTable(NPObject *pObj, PNPPHandle *ppNewHandle)
{		
	NPPHandle h;
	h.pObj = pObj;
	h.hId = nppHandleTable.size();
	nppHandleTable.push_back(h);
	*ppNewHandle = &(nppHandleTable[h.hId]);
	return true;
}

bool getPointerFromHandleId(NppHandleType hId, NPObject **pObj)
{
	if(hId >= nppHandleTable.size())
		return false;
	*pObj = nppHandleTable[hId].pObj;
	return true;
}

std::string nppHandleToString(NppHandleType handle){
	char buffer[16];
	snprintf(buffer, 16, "%ld", handle);
	return std::string(buffer);
}