//
//  NpcadesBrowserViewController.h
//  npcades-browser
//
//  Created by Кондакова Татьяна Андреевна on 21.03.12.
//  Copyright 2012 CryptoPro Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CPROCSP/PaneViewController.h>
#import "NpPluginWebView.h"

@interface NpcadesBrowserViewController : UIViewController<UIWebViewDelegate, UITextFieldDelegate> {
	NpPluginWebView *webView;
	IBOutlet UIToolbar *toolbar;
	IBOutlet UITextField *addressBar;
	IBOutlet UIBarButtonItem *backButton;
	IBOutlet UIBarButtonItem *forwardButton;
	IBOutlet UIBarButtonItem *refreshButton;
	IBOutlet UIActivityIndicatorView *activityIndicator;
	PaneViewController *CPROPane;
}

-(IBAction) startPane;
-(IBAction) gotoAddress;
-(IBAction) goBack;
-(IBAction) goForward;
@end

