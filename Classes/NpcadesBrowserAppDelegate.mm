//
//  NpcadesBrowserAppDelegate.mNpcadesBrowser
//  npcades-browser
//
//  Created by Кондакова Татьяна Андреевна on 21.03.12.
//  Copyright 2012 CryptoPro Ltd. All rights reserved.
//

#import "NpcadesBrowserAppDelegate.h"
#import "NpcadesBrowserViewController.h"

@implementation NpcadesBrowserAppDelegate

@synthesize window;
@synthesize viewController;


#pragma mark -
#pragma mark Application lifecycle

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {    
    
    // Override point for customization after application launch.

    // Add the view controller's view to the window and display.
    [window setRootViewController:viewController];
    [window makeKeyAndVisible];
    
    
    NSArray* keys =
    [[NSBundle mainBundle] URLsForResourcesWithExtension:@"key"
                                            subdirectory:nil];
    for (NSURL* url in keys) {
        NSString* filename = url.pathComponents.lastObject;
        NSString* name = [filename stringByDeletingPathExtension];
        NSError* error;
        if (![self copyKeyFile:name error:&error]) {
            NSLog(@"%@", error);
        }
    }

    return YES;
}

-(BOOL)copyKeyFile:(NSString*)name error:(NSError**)error {
    NSURL* documentsPath =
    [[NSFileManager defaultManager] URLsForDirectory:NSDocumentDirectory
                                           inDomains:NSUserDomainMask].firstObject;
    NSString* filePath = [NSString stringWithFormat:@"cprocsp/keys/mobile/%@.key", name];
    NSString* headerDst = [documentsPath.path stringByAppendingPathComponent:filePath];
    
    NSString* headerSrc =
    [[NSBundle mainBundle] pathForResource:name
                                    ofType:@"key"];
    
    [[NSFileManager defaultManager] removeItemAtPath:headerDst
                                               error:nil];
    
    return [[NSFileManager defaultManager] copyItemAtPath:headerSrc
                                                   toPath:headerDst
                                                    error:error];
    
}

- (void)applicationWillResignActive:(UIApplication *)application {
    /*
     Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
     Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
     */
}


- (void)applicationDidEnterBackground:(UIApplication *)application {
    /*
     Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later. 
     If your application supports background execution, called instead of applicationWillTerminate: when the user quits.
     */
}


- (void)applicationWillEnterForeground:(UIApplication *)application {
    /*
     Called as part of  transition from the background to the inactive state: here you can undo many of the changes made on entering the background.
     */
}


- (void)applicationDidBecomeActive:(UIApplication *)application {
    /*
     Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
     */
}


- (void)applicationWillTerminate:(UIApplication *)application {
    /*
     Called when the application is about to terminate.
     See also applicationDidEnterBackground:.
     */
}


#pragma mark -
#pragma mark Memory management

- (void)applicationDidReceiveMemoryWarning:(UIApplication *)application {
    /*
     Free up as much memory as possible by purging cached data objects that can be recreated (or reloaded from disk) later.
     */
}


- (void)dealloc {
    [viewController release];
    [window release];
    [super dealloc];
}


@end
