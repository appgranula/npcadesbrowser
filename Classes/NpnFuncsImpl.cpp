/*
 *  NpnFuncsImpl.cpp
 *  npcades-browser
 *
 *  Created by Кондакова Татьяна Андреевна on 08.02.12.
 *  Copyright 2012 CryptoPro Ltd. All rights reserved.
 *
 */

#include "NpnFuncsImpl.h"
#include "IdentifierRep.h"
#include "NpnObjCImpl.h"
#include <stdlib.h>
#include "NPJavaScriptObject.h"

NPObject npnvWindow;
NPObject npnvDocument;

NPIdentifier NpnImpl_GetStringIdentifier(const NPUTF8 *name)
{
	return static_cast<NPIdentifier>(IdentifierRep::get(std::string(name)));
}

void NpnImpl_GetStringIdentifiers(const NPUTF8 **names, int32_t nameCount, 
				NPIdentifier *identifiers)
{
	if(names && identifiers){
		for(int i = 0; i < nameCount; ++i)
			identifiers[i] = NpnImpl_GetStringIdentifier(names[i]); 
	}
}

NPIdentifier NpnImpl_GetIntIdentifier(int32_t intid)
{
	return static_cast<NPIdentifier>(IdentifierRep::get(intid));
}

bool NpnImpl_IdentifierIsString(NPIdentifier identifier)
{
	return static_cast<IdentifierRep *>(identifier)->isString();
}

NPUTF8 *NpnImpl_UTF8FromIdentifier(NPIdentifier identifier)
{
	const char* string = static_cast<IdentifierRep *>(identifier)->string().c_str();
	if(!string)
		return 0;
	return strdup(string);
}

int32_t NpnImpl_IntFromIdentifier(NPIdentifier identifier)
{
	return static_cast<IdentifierRep *>(identifier)->number();
}

NPObject *NpnImpl_CreateObject(NPP npp, NPClass *aClass)
{
	if(aClass){
		NPObject *obj;
		if(aClass->allocate != NULL)
			obj = aClass->allocate(npp, aClass);
		else 
			obj = (NPObject *)malloc(sizeof(NPObject));
		if(obj){
			obj->_class=aClass;
			obj->referenceCount = 1;
			return obj;
		}
	}
	return 0;
}

NPObject *NpnImpl_RetainObject(NPObject *npobj)
{
	if(npobj)
		npobj->referenceCount++;
	
	return npobj;
}

void NpnImpl_ReleaseObject(NPObject *npobj)
{
	if(npobj && npobj->referenceCount >= 1){
		if(--npobj->referenceCount == 0)
			if(npobj->_class->deallocate != NULL)
				npobj->_class->deallocate(npobj);
	}
		
}

bool NpnImpl_Invoke(NPP npp, NPObject *npobj, NPIdentifier methodName,
                const NPVariant *args, uint32_t argCount, NPVariant *result)
{
	if (npobj == &npnvWindow){
		if (argCount == 1 && NPVARIANT_IS_STRING(args[0])){
			int script_len = args[0].value.stringValue.UTF8Length;
			char * script = (char *)malloc(script_len + 1);
			strncpy(script, args[0].value.stringValue.UTF8Characters, script_len);
			script[script_len] = '\0';
			JSValueRef ref = 0;
			
			if(methodName == NpnImpl_GetStringIdentifier("eval")){	
				ref = evaluateInWindow(script);
				NPObject * jsobj = NpnImpl_CreateObject(npp, &NPJavaScriptObject_NPClass);
				NPVariant np_js_handle;
				INT32_TO_NPVARIANT((int)ref, np_js_handle);
				NpnImpl_SetProperty(npp, jsobj, NpnImpl_GetStringIdentifier("handle"), &np_js_handle);
				OBJECT_TO_NPVARIANT(jsobj, *result);
				free(script);
				return true;
			}
			else if (methodName == NpnImpl_GetStringIdentifier("confirm")){
				bool confirmed = confirmInWindow(script);
				BOOLEAN_TO_NPVARIANT(confirmed, *result);
				free(script);
				return true;
			}
		}
		
		return false;
	}
	
	if(npobj->_class->hasProperty(npobj, NpnImpl_GetStringIdentifier("isJSObject")))
		return npobj->_class->invoke(npobj, methodName, args, argCount, result);
	
	if(npobj->_class->invoke != NULL)
		if(npobj->_class->hasMethod(npobj, methodName))
			return npobj->_class->invoke(npobj, methodName, args, argCount, result);
	return false;
}

bool NpnImpl_InvokeDefault(NPP, NPObject *npobj, const NPVariant *args,
                       uint32_t argCount, NPVariant *result)
{
	if(npobj->_class->invokeDefault != NULL)
		return npobj->_class->invokeDefault(npobj, args, argCount, result);
	return false;
}

//see ObjC implementation
//bool NpnImpl_Evaluate(NPP npp, NPObject *npobj, NPString *script,
//                  NPVariant *result);

bool NpnImpl_GetProperty(NPP, NPObject *npobj, NPIdentifier propertyName,
						 NPVariant *result)
{
	if(npobj == &npnvWindow){
		if(propertyName == NpnImpl_GetStringIdentifier("document")){
			NpnImpl_RetainObject(&npnvDocument);
			NpnImpl_RetainObject(&npnvDocument);
			OBJECT_TO_NPVARIANT(&npnvDocument, *result);
			return true;
		}
		return false;
	}
	if(npobj == &npnvDocument){
		char * out  = getDocumentProperty(propertyName);
		STRINGZ_TO_NPVARIANT(out, *result);
		return true;
	}
	if(npobj->_class->hasProperty != NULL && npobj->_class->getProperty != NULL)
		if(npobj->_class->hasProperty(npobj, propertyName))
			return npobj->_class->getProperty(npobj, propertyName, result);
	return false;
}

bool NpnImpl_SetProperty(NPP, NPObject *npobj, NPIdentifier propertyName,
                     const NPVariant *value)
{
	if(npobj->_class->hasProperty != NULL && npobj->_class->setProperty != NULL)
		if(npobj->_class->hasProperty(npobj, propertyName))
			return npobj->_class->setProperty(npobj, propertyName, value);
	return false;
}

bool NpnImpl_RemoveProperty(NPP, NPObject *npobj, NPIdentifier propertyName)
{
	if(npobj->_class->removeProperty != NULL)
		return npobj->_class->removeProperty(npobj,propertyName);
	return false;
}

bool NpnImpl_HasProperty(NPP, NPObject *npobj, NPIdentifier propertyName)
{
	if(npobj->_class->hasProperty != NULL)
		return npobj->_class->hasProperty(npobj,propertyName);
	return false;
}

bool NpnImpl_HasMethod(NPP, NPObject *npobj, NPIdentifier methodName)
{
	if(npobj->_class->hasMethod != NULL)
		return npobj->_class->hasMethod(npobj,methodName);
	return false;
}

bool NpnImpl_Enumerate(NPP, NPObject *npobj, NPIdentifier **identifier,
                   uint32_t *count)
{
	if(npobj->_class->enumerate != NULL)
		return npobj->_class->enumerate(npobj, identifier, count);
	return false;
}

bool NpnImpl_Construct(NPP, NPObject *npobj, const NPVariant *args,
                   uint32_t argCount, NPVariant *result)
{
	if(npobj->_class->construct != NULL)
		return npobj->_class->construct(npobj, args, argCount, result);
	return false;
}

//not implemented yet
void NpnImpl_SetException(NPObject *npobj, const NPUTF8 *message)
{
	throwJsException(message);
	return;
}

const char * NpnImpl_UserAgent(NPP)
{
	return "npcades ios webkit";
}

NPError NpnImpl_GetValue(NPP npp, NPNVariable variable, void * value)
{
	switch (variable) {
		case NPNVWindowNPObject:{
			NpnImpl_RetainObject(&npnvWindow);
			NpnImpl_RetainObject(&npnvWindow);
			NPObject** pObj = (NPObject **)value;
			*pObj = &npnvWindow;
			break;
		}
		default:
			return NPERR_GENERIC_ERROR;
	}
	return NPERR_NO_ERROR;
}

void* NpnImpl_MemAlloc(uint32_t size)
{
	return malloc(size);
}

void NpnImpl_ReleaseVariantValue(NPVariant* variant)
{
	if(variant->type == NPVariantType_Object){
		NpnImpl_ReleaseObject(variant->value.objectValue);
		variant->value.objectValue = 0;
	}
	else if (variant->type == NPVariantType_String){
		free((void *)variant->value.stringValue.UTF8Characters);
		variant->value.stringValue.UTF8Characters = 0;
		variant->value.stringValue.UTF8Length = 0;
	}
	variant->type = NPVariantType_Void;
}