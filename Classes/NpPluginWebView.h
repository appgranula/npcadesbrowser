//
//  NpPluginWebView.h
//  npcades-browser
//
//  Created by Кондакова Татьяна Андреевна on 08.02.12.
//  Copyright 2012 CryptoPro Ltd. All rights reserved.
//

#import "SBJSON.h"
#import "npfunctions.h"

@interface NpPluginWebView : UIWebView <UIWebViewDelegate> {
	SBJSON *json;
	int currentCallbackId;
	NPPluginFuncs pPluginFuncs;
	NPNetscapeFuncs pNetscapeFuncs;
	NPP npp;
	NPObject *scriptableMainObj;
}

- (void)handleCall:(NSString*)functionName callbackId:(int)callbackId args:(NSArray*)args;
- (void)returnResult:(int)callbackId exception:(NSString *)message args:(NPVariant) arg;
- (void)initializeNpnFuncs;
- (void)CreateObject: (int)callbackId args:(NSArray*)args;
- (void)GetProperty: (int)callbackId args:(NSArray*)args;
- (void)SetProperty: (int)callbackId args:(NSArray*)args;
- (void)Invoke: (int)callbackId args:(NSArray*)args;

@end
