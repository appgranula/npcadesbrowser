������ ������������� npcades �� iOS 
������ ��������� ������� �������, ���������� ����������������� �������� � ��������
�������� ������� �� ���-���������; ��������� �������� �� iOS ���������������� 
��� browser plugin: http://cryptopro.ru/products/cades/plugin .

________________________________________________________________________________

������ ������� �������������� � ������������ � ����������� �� ������ �������� �� 
������� ����������.

������������� �����������:
- �������� � ������ ������� ������������ ����� �� ���������� npapi, �������� �
  ������ ����������
- �������� � ������ ���������� libpki.o, libnpcades.o, libcppcades.o, �������� �
  ������ ����������
- ���� ��� ������ �� ����� ������� ������������ ����� libxml, �������� � 
  "header search path" ���� � ������������ ����� libxml. ��� ��������� ������������
  �����, �� ��������� ����� ����������� � ����������� �� ����� ������ �� � SDK.
  ��������, ��� ����� ���������� � /usr/include/libxml2/ ��� � /Applications/Xcode.app/
  Contents/Developer/Platforms/iPhoneOs.platform/Developer/SDKs/iPhoneOS5.1.sdk/usr/
  include/libxml2/ . ���� �� �� ������ ��� ��������� ��� ������������ �����, ����������
  ����������� ����� � 
  /Applications/Xcode.app/Contents/Developer/Platforms/iPhoneOs.platform/Developer
  /Developer/Platforms/iPhoneOS.platform
  /usr/include
  /Applications/Xcode.app/Contents/Developer/
  /Developer
 
________________________________________________________________________________
  
��� ������ ��������, ���������� ��� ��� browser plugin 
( http://cryptopro.ru/products/cades/plugin ) � ������� ��� iOS �����������:

- �������� � ��� �������� IOS_npcades_supp.js (������������ � ������� ����� �������)
- �������������� ������� CreateObject ��������� �������:

function CreateObject(name) {
  switch (navigator.appName) {
    case "Microsoft Internet Explorer":
      return new ActiveXObject(name);
    default:
      var userAgent = navigator.userAgent;
      if(userAgent.match(/ipod/i) ||
         userAgent.match(/ipad/i) ||
         userAgent.match(/iphone/i)){
        return call_ru_cryptopro_npcades_10_native_bridge("CreateObject", [name]);
      }
      var cadesobject = document.getElementById("cadesplugin");
      return cadesobject.CreateObject(name);
  }
}

________________________________________________________________________________

�������� ��������: � ������� ��� iOS �� �������������� ������ �� ���������. ��� 
������ ���������� �������� ����.  
________________________________________________________________________________
  
 ������ ��������������� ���������.
 ��������� ����� �� ������ ����������� ��� ������-���.
